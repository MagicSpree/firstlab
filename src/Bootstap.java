public class Bootstap {
    public static void main(String[] args) {
        int firstValue = 11;
        double secondValue = 5.2;

        Calculator calc = new Calculator();
        calc.printResult(firstValue, secondValue);
    }
}
