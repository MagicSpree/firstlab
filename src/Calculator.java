/**
 * Java Core1
 * Основы 1. Структура программы, синтаксис, типы данных
 * <p>
 * Лабораторная работа
 */
public class Calculator {
    public void printResult(int firstValue, double secondValue) {
        println(sum(firstValue, secondValue));
        println(subtraction(firstValue, secondValue));
        println(multiplication(firstValue, secondValue));
        println(division(firstValue, secondValue));
        println(exponentiation(firstValue, secondValue));
        println(divisionWithRemainder(firstValue, secondValue));
    }

    private void println(double result) {
        System.out.println(result);
    }

    private double sum(int a, double b) {
        return a + b;
    }

    private double subtraction(int a, double b) {
        return a - b;
    }

    private double multiplication(int a, double b) {
        return a * b;
    }

    private double division(int a, double b) {
        return a / b;
    }

    private double exponentiation(int a, double b) {
        return Math.pow(b, a);
    }

    private double divisionWithRemainder(int a, double b) {
        return a % b;
    }
}